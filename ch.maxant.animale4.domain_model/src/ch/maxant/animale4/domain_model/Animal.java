/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.domain_model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Animal implements Serializable, Comparable<Animal> {

	private static final long serialVersionUID = 1L;

	private static AtomicInteger nextId = new AtomicInteger(1);

	private String name;
	private int age;
	private int id = nextId();

	private BigDecimal price;

	public String getName() {
		return name;
	}

	private synchronized static int nextId() {
		return nextId.incrementAndGet();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public int getAge() {
		return age;
	}
	
	public abstract String sayName();

	@Override
	public int compareTo(Animal o) {
		if(name != null){
			return name.compareTo(o.name);
		}else{
			if(o.name == null){
				return 0;
			}else{
				return -1;
			}
		}
	}

	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Animal){
			Animal a = (Animal) obj;
			if(id == a.id){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return 
				"{Animal:" +
					"{id:" + id + "," +
					 "name:" + name + "," +
					 "age:" + age + "," +
					 "price:" + price + "}" +
				 "}";
	}
}
