/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.eclipse.e4.core.services.events.IEventBroker;

import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.model.IModel;
import ch.maxant.animale4.model.Model;
import ch.maxant.animale4.service.AnimalDoesNotExistException;
import ch.maxant.animale4.service.AnimalService;
import ch.maxant.animale4.service.IdRequiredException;
import ch.maxant.animale4.servicelocation.IServiceLocator;

@Singleton
public class Controller {

	/** the name of the topic on which model events for this app are fired */
	public static final String TOPIC = "ch/maxant/animale4/core/controller/model/events";

	/** a service which the client needs to use */
	private AnimalService service;

	/**
	 * we need a ref to the more complex model impl, rather than the read only
	 * {@link IModel} interface.
	 * 
	 * note though, its not injected! The context only knows {@link IModel}, so
	 * we need to cast it into something more useful. see {@link #init(IModel)}
	 * - thats where we do the cast.
	 */
	private Model model;

	@Inject
	private IEventBroker eventBroker;

	@Inject
	private IServiceLocator locator;

	@PostConstruct
	public void init(IModel model) {
		this.model = (Model) model;
		locateService();
		initModel();
		bindModel();
	}

	private void initModel() {
		model.setMasterData(service.getMasterData());
	}

	private void bindModel() {
		//we want the model to be independent of eclipse.
		//because of that it cannot know the event broker.
		//in order to send events, the model uses Java Beans
		//property support and the following listener pushes 
		//them onto the eclipse event broker so that we can
		//use it and all its advantages, rather than our own
		//firing mechanism.
		model.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				eventBroker.send(TOPIC, evt);
			}
		});
	}

	private void locateService() {
		service = locator.getService(AnimalService.class);
	}

	/**
	 * loads the model from the server and sticks it in the model, ready for
	 * displaying
	 */
	public void loadAnimals() {
		List<Animal> animals = service.getAllAnimals();
		model.setAllAnimals(animals);
	}

	/**
	 * sell the given animal.  it must NOT exist in the model!
	 */
	public void sellAnimal(Animal a) {
		
		service.sellAnimal(a);

		//now is a great time to refresh the model from the server - we want the available animals
		//which is now different because we bought one, and potentially others have bought animals too
		loadAnimals();
	}

	public boolean purchaseAnimal(Animal currentSelection, IdProvider view) throws IdRequiredException, AnimalDoesNotExistException {
		String passportNumber = null;
		double max = model.getMasterData()
				.getPriceThresholdWherePurchasesRequireId().doubleValue();
		double price = currentSelection.getPrice().doubleValue();
		if (price > max) {

			// we need to send the passport number to the server too!
			// sure, we could just let the server throw an exception, but
			// this is a bit of business logic which our customer wants on the
			// client, so that the user doesn't waste time waiting for a
			// response from the server.
			passportNumber = view.getIdNumber("Price is above threshold");

			// we wont validate it here, nor is it validated in the view,
			// just to show what happens if the exception is thrown
			
			if(view.isUserCancelled()){
				return false;
			}
		}
		
		service.purchaseAnimal(currentSelection, passportNumber);
		
		//now is a great time to refresh the model from the server - we want the available animals
		//which is now different because we bought one, and potentially others have bought animals too
		loadAnimals();
		
		return true;
	}
}
