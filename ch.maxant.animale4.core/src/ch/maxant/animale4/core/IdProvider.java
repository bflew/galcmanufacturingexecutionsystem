/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.core;

/**
 * the controller needs to be independent of the view implementation.
 * sometimes, a controller needs to do something viewey, like
 * get extra information from the user.  instead of sending an event to do that, 
 * it can use this interface.  this interface allows the controller
 * and the view implementation to be decoupled.  the controller 
 * doesn't need to know the implementation, just that there is a method
 * it can call, if required, to do something with the view.
 * the interface itself is also UI independent - its just a bunch
 * of methods which the controller needs any view which uses it to, 
 * implement so that it can function fully.<br>
 * <br>
 * I guess other ways to do this are to throw (business) exceptions 
 * from the controller which the view can react to, or for the 
 * controller to send an event to the view which causes it to 
 * do something viewey.  With either of these cases, the view cannot
 * return information to the controller.<br>
 * <br>
 * Lets say during validation, you determine that the price of something
 * is above a threshold, and you need to additionally collect
 * the customers ID (say passport) number.  you could throw a
 * ThresholdExceededException so that the view can go get 
 * the passport number from the user, but the view then has
 * to call the controller again, and its messy.  same is true of
 * firing an event - thats really messy.<br>
 * <br>
 * If the controller can make callbacks to the view to get additional
 * information which it needs using an interface like this, you can really
 * easily read through the controller code to get an understanding of 
 * what is going on in the process it implements.<br>
 * <br>
 * some might say that logic like this (validation) belongs on the server.
 * it does, but it may <i>also</i> belong on the client, if the requirement
 * is to very quickly allow the sales agent to add the additional information
 * the system requires, without first making a relatively slow server call.
 * ive worked on systems where every second wasted by an agent causes the 
 * queue they are serving to grow. when you're in a supermarket, you don't 
 * want to wait unnecessarily long, because the thousands of tills in the 
 * nationwide system do all their validation on the server...<br>
 * <br>
 * note: while the name doesn't have anything to do with a view,
 * the provider is implicitly a view component, because the user
 * has to provide this info.
 * others might give this interface a different name, like
 * "IdNumberProvider" - sure, that works too.  I've explicitly 
 * called it a view here, to make it clear that the user is going
 * to enter this number over the UI and as such it's implementation
 * will depend on something like SWT or whatever base technology is used
 * for the rest of the UI.  of course, there are those people who would
 * say that the number just needs to come from somewhere - i shouldn't 
 * dictate from here, that it will come from a view.  I guess the whole
 * point is simply that you need to decouple the controller from the view
 * and this is a great way of doing it.  The controller <b>NEVER</b> needs
 * to know the view!
 */
public interface IdProvider {

	String getIdNumber(String reasonToDisplay);
	
	boolean isUserCancelled();
	
}
