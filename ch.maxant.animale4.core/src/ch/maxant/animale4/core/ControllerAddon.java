/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.core;

import javax.annotation.PostConstruct;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;

/**
 * this class is here so that the controller can be created and 
 * set into the eclipse context, so that it can be injected into other 
 * classes that need it.
 */
public class ControllerAddon {

	@PostConstruct
	void hookListeners(IEclipseContext context) {

		//instantiate and setup the controller here - it can be injected everywhere that is required.
		//the model is instantiated using DS.  It is injected into the controller.
		//if i only need this controller within a given single part, then it 
		Controller controller = ContextInjectionFactory.make(Controller.class, context);

		//stick it in the context, so that others can get it injected
		context.set(Controller.class, controller);
	}
}