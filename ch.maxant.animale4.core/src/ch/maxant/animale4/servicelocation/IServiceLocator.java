/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.servicelocation;

/**
 * the standard design pattern!<br>
 * <br>
 * well, the core bundle declares this interface.  the controller needs an instance
 * of the service locator in order to find services it can use.<br>
 * <br>
 * the implementation of the interface is a deployment issue!  a different 
 * bundle provides a declarative service which implements this interface.
 * OSGi is used to resolve the service locator implementation, and the e4 EAP
 * injects the service locator into the controller.<br>
 * <br>
 * Note: this interface is actually entirely independent of the animale4 application
 * and could be stuck in its own core bundle.
 */
public interface IServiceLocator {

	/** goes and locates the service */
	<T> T getService(Class<T> clazz);
}
