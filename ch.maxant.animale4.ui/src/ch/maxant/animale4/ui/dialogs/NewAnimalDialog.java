/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.ui.dialogs;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.domain_model.Dog;
import ch.maxant.animale4.ui.util.SelectionAdapter;

public class NewAnimalDialog extends Dialog {

	private Text name;
	private Text age;
	private Text price;
	private String type;
	
	/** create a temp instance used by databinding to store values */
	@SuppressWarnings("serial")
	private Animal animal = new Animal(){
		@Override
		public String sayName() {
			return null;
		}
	};
	private Combo typeCombo;
	
	public NewAnimalDialog(Shell parent) {
		super(parent);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = (Composite) super.createDialogArea(parent);

		buildUI(c);
		bindUI();
		
		return c;
	}
		
	private void bindUI() {
		
		DataBindingContext bindingContext = new DataBindingContext();
		
		{
			IObservableValue widgetValue = WidgetProperties.text(SWT.Modify).observe(name);
			IObservableValue modelValue = PojoProperties.value(Dog.class, "name").observe(animal);
			bindingContext.bindValue(widgetValue, modelValue);
		}

		{
			IObservableValue widgetValue = WidgetProperties.text(SWT.Modify).observe(age);
			IObservableValue modelValue = PojoProperties.value(Dog.class, "age").observe(animal);
			bindingContext.bindValue(widgetValue, modelValue);
			
			IValidator numberValidator = new IValidator() {
				
				@Override
				public IStatus validate(Object value) {
					String s = String.valueOf(value);
					boolean matches = s.matches("\\d*");
					if (matches) {
						return ValidationStatus.ok();
					}
					return ValidationStatus.error("Only Number permitted");
				}
			};
			UpdateValueStrategy targetToModel = new UpdateValueStrategy();
			targetToModel.setBeforeSetValidator(numberValidator);
			
			Binding bindValue = bindingContext.bindValue(widgetValue, modelValue, targetToModel, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.RIGHT);
		}
		
		{
			IObservableValue widgetValue = WidgetProperties.text(SWT.Modify).observe(price);
			IObservableValue modelValue = PojoProperties.value(Dog.class, "price").observe(animal);
			bindingContext.bindValue(widgetValue, modelValue);
			
			IValidator numberValidator = new IValidator() {
				
				@Override
				public IStatus validate(Object value) {
					String s = String.valueOf(value);
					boolean matches = s.matches("\\d*");
					if (matches) {
						return ValidationStatus.ok();
					}
					return ValidationStatus.error("Only Number permitted");
				}
			};
			UpdateValueStrategy targetToModel = new UpdateValueStrategy();
			targetToModel.setBeforeSetValidator(numberValidator);
			
			Binding bindValue = bindingContext.bindValue(widgetValue, modelValue, targetToModel, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.RIGHT);
		}
		
		typeCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void selected(SelectionEvent e) {
				type = typeCombo.getItem(typeCombo.getSelectionIndex());
			}
		});
		
	}

	private void buildUI(Composite parent){
		Composite c = new Composite(parent, SWT.NONE);
		c.setLayout(new GridLayout(2, false));

		{
			typeCombo = new Combo(c, SWT.DROP_DOWN | SWT.READ_ONLY);
			typeCombo.add("Dog");
			typeCombo.add("Cat");
			typeCombo.select(0);
			type = typeCombo.getItem(typeCombo.getSelectionIndex()); //set the initial value

			GridData gd = new GridData();
			gd.horizontalSpan = 2;
			typeCombo.setLayoutData(gd);
		}
		
		{
			Label l = new Label(c, SWT.NONE);
			l.setText("Name:");
			
			name = new Text(c, SWT.BORDER);
			GridData gd = new GridData();
			gd.widthHint = 200;
			name.setLayoutData(gd);
		}
		
		{
			Label l = new Label(c, SWT.NONE);
			l.setText("Age:");
			
			age = new Text(c, SWT.BORDER);
			GridData gd = new GridData();
			gd.widthHint = 200;
			age.setLayoutData(gd);
		}

		{
			Label l = new Label(c, SWT.NONE);
			l.setText("Price:");
			
			price = new Text(c, SWT.BORDER);
			GridData gd = new GridData();
			gd.widthHint = 200;
			price.setLayoutData(gd);
		}
	}

	public Animal getAnimal() {
		
		//we just have a really abstract one right now - turn it into a concrete impl!
		
		String className = Animal.class.getPackage().getName();
		className += "." + type.substring(0, 1).toUpperCase() + type.substring(1);
		Animal ret;
		try {
			ret = (Animal) Class.forName(className).newInstance();
		} catch (Exception e) {
			throw new RuntimeException("programmer error", e);
		}
		ret.setAge(animal.getAge());
		ret.setName(animal.getName());
		ret.setPrice(animal.getPrice());

		return ret;
	}
}
