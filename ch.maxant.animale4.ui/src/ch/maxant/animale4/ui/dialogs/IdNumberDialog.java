/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class IdNumberDialog extends Dialog {

	private Text idNumber;
	private String number;
	
	public IdNumberDialog(Shell parent) {
		super(parent);
		parent.setText("ID or Passport Number");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = (Composite) super.createDialogArea(parent);

		buildUI(c);
		
		return c;
	}
		
	private void buildUI(Composite parent){
		Composite c = new Composite(parent, SWT.NONE);
		c.setLayout(new GridLayout(2, false));

		{
			Label l = new Label(c, SWT.NONE);
			l.setText("Please provide the following details for identity verification.");
			
			GridData gd = new GridData();
			gd.horizontalSpan = 2;
			l.setLayoutData(gd);
		}
		
		{
			Label l = new Label(c, SWT.NONE);
			l.setText("ID Number:");

			idNumber = new Text(c, SWT.BORDER);
			GridData gd = new GridData();
			gd.widthHint = 200;
			idNumber.setLayoutData(gd);
			
			idNumber.addModifyListener(new ModifyListener() {
				@Override
				public void modifyText(ModifyEvent e) {
					number = idNumber.getText();
				}
			});
			
			//didnt add a validator on purpose - i want to show what happens
			//when the server throws an exception!
		}
	}

	public String getNumber() {
		return number;
	}
}
