/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.ui.parts;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import ch.maxant.animale4.core.Controller;
import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.model.IModel;

public class AnimalListPart  implements EventHandler {
	
	@Inject
	private IModel model;

	@Inject
	private ESelectionService selectionService;

	private TableViewer viewer;

	private final MApplication application;
	
	@Inject
	public AnimalListPart(IEventBroker eventBroker, MApplication application) {
		this.application = application;
		eventBroker.subscribe(Controller.TOPIC, this);
	}
	
	
	@PostConstruct
	public void postConstruct(Composite parent, IEclipseContext context) {
		
		//dont do this here, unless this part is the only user of this controller
		//for shared ones, do it in the addon
		//controller = ContextInjectionFactory.make(Controller.class, context);
		
		buildUI(parent);
		bindUI();
	}
	
	private void buildUI(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		
		buildList(parent);
	}
	
	private void buildList(Composite parent) {
		Table t = new Table(parent, SWT.H_SCROLL|SWT.V_SCROLL|SWT.BORDER|SWT.SINGLE|SWT.FULL_SELECTION);
		
		GridData gd = new GridData();
		gd.horizontalAlignment = SWT.FILL;
		gd.verticalAlignment = SWT.FILL;
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = true;
		t.setLayoutData(gd);
		
		viewer = new TableViewer(t);
		
		ArrayContentProvider provider = new ArrayContentProvider();
		viewer.setContentProvider(provider);
		
		viewer.setLabelProvider(new LabelProvider(){
			@Override
			public String getText(Object element) {
				return ((Animal)element).getName();
			}
		});
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection sel = (IStructuredSelection) event.getSelection();
				Animal a = (Animal) sel.getFirstElement();
				if(selectionService != null){
					selectionService.setSelection(a);
				}
			}
		});
	}

	private void bindUI() {
	}

	@Focus
	public void onFocus() {
	}
	
	
	@Persist
	public void save() {
	}
	
	@Override
	public void handleEvent(Event event) {

		PropertyChangeEvent pce = (PropertyChangeEvent) event.getProperty(IEventBroker.DATA);
		
		if(IModel.ADDED_ANIMALS.equals(pce.getPropertyName())){
			
			List<Animal> animals = new ArrayList<Animal>(model.getAnimals());
			Collections.sort(animals);
			
			viewer.setInput(animals.toArray());

			setTitle();
		}
	}
	
	private void setTitle(){
		String title = "Animals";
		if(model.isDirty()){
			title += " *";
		}

		//the traditional way to set the title
		Display.getCurrent().getActiveShell().setText(title );

		//using the app XMI model to set the title
		for(MWindow w : application.getChildren()){
			w.setLabel(title);
		}
	}
	
}