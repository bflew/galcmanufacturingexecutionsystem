/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.ui.parts;

import java.lang.reflect.UndeclaredThrowableException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import ch.maxant.animale4.core.Controller;
import ch.maxant.animale4.core.IdProvider;
import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.service.AnimalDoesNotExistException;
import ch.maxant.animale4.service.IdRequiredException;
import ch.maxant.animale4.ui.dialogs.IdNumberDialog;
import ch.maxant.animale4.ui.util.SelectionAdapter;

/**
 * this is part of the UI, or view in MVC.
 */
public class AnimalDetailsPart implements ISelectionListener {

	@Inject
	private Controller controller;
	
	@Inject
	private ESelectionService selectionService;

	private Label nameLabel;
	private Label ageLabel;
	private Label helloLabel;
	private Label priceLabel;
	private Label typeLabel;
	private Animal currentSelection;
	private Button purchase;

	@Inject
	private IEclipseContext ctx;

	@Inject
	public AnimalDetailsPart() {
	}
	
	@PostConstruct
	public void postConstruct(Composite parent) {
		buildUI(parent);
		bindUI();
	}

	private void bindUI() {
		selectionService.addSelectionListener(this);
	}
	
	@Override
	public void selectionChanged(MPart part, Object selection) {
		if(selection instanceof Animal){
			if(selection != null){
				currentSelection = (Animal) selection;

				nameLabel.setText(currentSelection.getName());
				ageLabel.setText(String.valueOf(currentSelection.getAge()));
				helloLabel.setText(currentSelection.sayName());
				priceLabel.setText(String.valueOf(currentSelection.getPrice()) + " CHF");
				typeLabel.setText(currentSelection.getClass().getSimpleName());

				purchase.setEnabled(true);
			}
		}
	}

	private void buildUI(Composite parent) {
		
		parent.setLayout(new GridLayout(2, false));
		
		{
			Label l = new Label(parent, SWT.NONE);
			l.setText("Type");
			
			typeLabel = new Label(parent, SWT.NONE);
			GridData gd = new GridData();
			gd.widthHint = 200;
			typeLabel.setLayoutData(gd);
		}
		
		{
			Label l = new Label(parent, SWT.NONE);
			l.setText("Name");
			
			nameLabel = new Label(parent, SWT.NONE);
			GridData gd = new GridData();
			gd.widthHint = 200;
			nameLabel.setLayoutData(gd);
		}
		
		{
			Label l = new Label(parent, SWT.NONE);
			l.setText("Age");
			
			ageLabel = new Label(parent, SWT.NONE);
			GridData gd = new GridData();
			gd.widthHint = 200;
			ageLabel.setLayoutData(gd);
		}
		
		{
			Label l = new Label(parent, SWT.NONE);
			l.setText("Hello");
			
			helloLabel = new Label(parent, SWT.NONE);
			GridData gd = new GridData();
			gd.widthHint = 200;
			helloLabel.setLayoutData(gd);
		}
		
		{
			Label l = new Label(parent, SWT.NONE);
			l.setText("Price");
			
			priceLabel = new Label(parent, SWT.NONE);
			GridData gd = new GridData();
			gd.widthHint = 200;
			priceLabel.setLayoutData(gd);
		}
		
		{
			purchase = new Button(parent, SWT.PUSH);
			purchase.setText("Purchase this animal");
			purchase.addSelectionListener(new SelectionAdapter() {
				@Override
				public void selected(SelectionEvent e) {
					handlePurchase();
				}
			});
			purchase.setEnabled(false);
		}
	}

	/** called by the purchase button to purchase the current selection */
	protected void handlePurchase() {
		Shell shell = Display.getCurrent().getActiveShell();
		try{
			boolean success = controller.purchaseAnimal(currentSelection, createIdProvider());
			if(success){
				MessageDialog.openWarning(shell, "Congratulations", "The " + currentSelection.getClass().getSimpleName().toLowerCase() + " is all yours!");
			}else{
				//false simply means that the user cancelled
			}
		}catch(IdRequiredException ex){
			MessageDialog.openWarning(shell, "Warning", "For purchases above " + ex.getThreshold() + " CHF you need to provide your ID number.");
		} catch (AnimalDoesNotExistException e) {
			MessageDialog.openError(shell, "Error", e.getAnimal().getName() + " has already been sold to someone else I'm afraid.");
		}catch(UndeclaredThrowableException ex){
			MessageDialog.openError(shell, "Error", "Failed to purchase the animal: " + ex.getCause().getMessage());
		}
	}

	/**
	 * @return a new {@link IdProvider}, as required by the controller
	 */
	private IdProvider createIdProvider() {
		return new IdProvider() {
			
			private boolean userCancelledIdNumber = false;
			
			@Override
			public boolean isUserCancelled() {
				return userCancelledIdNumber ;
			}
			
			@Override
			public String getIdNumber(String reasonToDisplay) {
				userCancelledIdNumber = false;
				IdNumberDialog d = new IdNumberDialog((Shell) ctx.get(IServiceConstants.ACTIVE_SHELL));
				if(d.open() == Dialog.OK){
					return d.getNumber();
				}
				userCancelledIdNumber = true;
				return null;
			}
		};
	}

}