package org.jboss.tools.portlet.ui;

import java.util.*;
import org.eclipse.jst.j2ee.internal.common.operations.*;
import org.jboss.tools.portlet.operations.*;

public class PortletTemplate
{
  protected static String nl;
  public static synchronized PortletTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    PortletTemplate result = new PortletTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "package ";
  protected final String TEXT_2 = ";";
  protected final String TEXT_3 = NL;
  protected final String TEXT_4 = NL + "import ";
  protected final String TEXT_5 = ";";
  protected final String TEXT_6 = NL;
  protected final String TEXT_7 = NL + "public ";
  protected final String TEXT_8 = "abstract ";
  protected final String TEXT_9 = "final ";
  protected final String TEXT_10 = "class ";
  protected final String TEXT_11 = " extends ";
  protected final String TEXT_12 = " implements ";
  protected final String TEXT_13 = ", ";
  protected final String TEXT_14 = " {";
  protected final String TEXT_15 = NL + NL + "    /**" + NL + "     * Default constructor. " + NL + "     */" + NL + "    public ";
  protected final String TEXT_16 = "() {" + NL + "        // TODO Auto-generated constructor stub" + NL + "    }";
  protected final String TEXT_17 = NL + "       " + NL + "    /**" + NL + "     * @see ";
  protected final String TEXT_18 = "#";
  protected final String TEXT_19 = "(";
  protected final String TEXT_20 = ")" + NL + "     */" + NL + "    public ";
  protected final String TEXT_21 = "(";
  protected final String TEXT_22 = ") {" + NL + "        super(";
  protected final String TEXT_23 = ");" + NL + "        // TODO Auto-generated constructor stub" + NL + "    }";
  protected final String TEXT_24 = NL + NL + "\t/**" + NL + "     * @see ";
  protected final String TEXT_25 = "#";
  protected final String TEXT_26 = "(";
  protected final String TEXT_27 = ")" + NL + "     */" + NL + "    public ";
  protected final String TEXT_28 = " ";
  protected final String TEXT_29 = "(";
  protected final String TEXT_30 = ") {" + NL + "        // TODO Auto-generated method stub";
  protected final String TEXT_31 = NL + "\t\t\treturn ";
  protected final String TEXT_32 = ";";
  protected final String TEXT_33 = NL + "    }";
  protected final String TEXT_34 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.Portlet#init()" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic void init() throws PortletException {" + NL + "\t\t// TODO Auto-generated method stub" + NL + "\t\tsuper.init();" + NL + "\t}";
  protected final String TEXT_35 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.Portlet#destroy()" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic void destroy() {" + NL + "\t\t// TODO Auto-generated method stub" + NL + "\t\tsuper.destroy();" + NL + "\t}";
  protected final String TEXT_36 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.GenericPortlet#getPortletConfig()" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic PortletConfig getPortletConfig() {" + NL + "\t\t// TODO Auto-generated method stub" + NL + "\t\treturn super.getPortletConfig();" + NL + "\t}";
  protected final String TEXT_37 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.GenericPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)" + NL + "\t */" + NL + "\t@Override" + NL + "\tprotected void doView(RenderRequest request, RenderResponse response)" + NL + "\t\t\tthrows PortletException, IOException, UnavailableException {" + NL + "\t\tresponse.setContentType(\"text/html\");" + NL + "\t\tPrintWriter writer = response.getWriter();" + NL + "\t\twriter.write(\"Hello World!\");" + NL + "\t\twriter.close();" + NL + "\t}";
  protected final String TEXT_38 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.GenericPortlet#doEdit(javax.portlet.RenderRequest, javax.portlet.RenderResponse)" + NL + "\t */" + NL + "\t@Override" + NL + "\tprotected void doEdit(RenderRequest request, RenderResponse response)" + NL + "\t\t\tthrows PortletException, PortletSecurityException, IOException {" + NL + "\t\t// TODO Auto-generated method stub" + NL + "\t\tsuper.doEdit(request, response);" + NL + "\t}";
  protected final String TEXT_39 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.GenericPortlet#doHelp(javax.portlet.RenderRequest, javax.portlet.RenderResponse)" + NL + "\t */" + NL + "\t@Override" + NL + "\tprotected void doHelp(RenderRequest request, RenderResponse response)" + NL + "\t\t\tthrows PortletException, PortletSecurityException, IOException {" + NL + "\t\t// TODO Auto-generated method stub" + NL + "\t\tsuper.doHelp(request, response);" + NL + "\t}";
  protected final String TEXT_40 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.GenericPortlet#doDispatch(javax.portlet.RenderRequest, javax.portlet.RenderResponse)" + NL + "\t */" + NL + "\t@Override" + NL + "\tprotected void doDispatch(RenderRequest arg0, RenderResponse arg1)" + NL + "\t\t\tthrows PortletException, PortletSecurityException, IOException {" + NL + "\t\t// TODO Auto-generated method stub" + NL + "\t\tsuper.doDispatch(arg0, arg1);" + NL + "\t}";
  protected final String TEXT_41 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.Portlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic void render(RenderRequest request, RenderResponse response)" + NL + "\t\t\tthrows PortletException, PortletSecurityException, IOException {" + NL + "\t\t// TODO Auto-generated method stub" + NL + "\t\tsuper.render(request, response);" + NL + "\t}";
  protected final String TEXT_42 = NL + NL + "\t/* (non-Javadoc)" + NL + "\t * @see javax.portlet.Portlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic void processAction(ActionRequest request, ActionResponse response)" + NL + "\t\t\tthrows PortletException, PortletSecurityException, IOException {" + NL + "\t\t// TODO Auto-generated method stub" + NL + "\t\tsuper.processAction(request, response);" + NL + "\t}";
  protected final String TEXT_43 = NL + "}";
  protected final String TEXT_44 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     CreatePortletTemplateModel model = (CreatePortletTemplateModel) argument; 
    
	model.removeFlags(CreateJavaEEArtifactTemplateModel.FLAG_QUALIFIED_SUPERCLASS_NAME); 

    
	if (model.getJavaPackageName() != null && model.getJavaPackageName().length() > 0) {

    stringBuffer.append(TEXT_1);
    stringBuffer.append( model.getJavaPackageName() );
    stringBuffer.append(TEXT_2);
    
	}

    stringBuffer.append(TEXT_3);
     
	Collection<String> imports = model.getImports();
	for (String anImport : imports) { 

    stringBuffer.append(TEXT_4);
    stringBuffer.append( anImport );
    stringBuffer.append(TEXT_5);
     
	}

    stringBuffer.append(TEXT_6);
    
	if (model.isPublic()) { 

    stringBuffer.append(TEXT_7);
     
	} 

	if (model.isAbstract()) { 

    stringBuffer.append(TEXT_8);
    
	}

	if (model.isFinal()) {

    stringBuffer.append(TEXT_9);
    
	}

    stringBuffer.append(TEXT_10);
    stringBuffer.append( model.getClassName() );
    
	String superClass = model.getSuperclassName();
 	if (superClass != null && superClass.length() > 0) {

    stringBuffer.append(TEXT_11);
    stringBuffer.append( superClass );
    
	}

	List<String> interfaces = model.getInterfaces(); 
 	if ( interfaces.size() > 0) { 

    stringBuffer.append(TEXT_12);
    
	}
	
 	for (int i = 0; i < interfaces.size(); i++) {
   		String INTERFACE = (String) interfaces.get(i);
   		if (i > 0) {

    stringBuffer.append(TEXT_13);
    
		}

    stringBuffer.append( INTERFACE );
    
	}

    stringBuffer.append(TEXT_14);
     
	if (!model.hasEmptySuperclassConstructor()) { 

    stringBuffer.append(TEXT_15);
    stringBuffer.append( model.getClassName() );
    stringBuffer.append(TEXT_16);
     
	} 

	if (model.shouldGenSuperclassConstructors()) {
		List<Constructor> constructors = model.getConstructors();
		for (Constructor constructor : constructors) {
			if (constructor.isPublic() || constructor.isProtected()) { 

    stringBuffer.append(TEXT_17);
    stringBuffer.append( model.getSuperclassName() );
    stringBuffer.append(TEXT_18);
    stringBuffer.append( model.getSuperclassName() );
    stringBuffer.append(TEXT_19);
    stringBuffer.append( constructor.getParamsForJavadoc() );
    stringBuffer.append(TEXT_20);
    stringBuffer.append( model.getClassName() );
    stringBuffer.append(TEXT_21);
    stringBuffer.append( constructor.getParamsForDeclaration() );
    stringBuffer.append(TEXT_22);
    stringBuffer.append( constructor.getParamsForCall() );
    stringBuffer.append(TEXT_23);
    
			} 
		} 
	} 

    
	if (model.shouldImplementAbstractMethods()) {
		for (Method method : model.getUnimplementedMethods()) { 

    stringBuffer.append(TEXT_24);
    stringBuffer.append( method.getContainingJavaClass() );
    stringBuffer.append(TEXT_25);
    stringBuffer.append( method.getName() );
    stringBuffer.append(TEXT_26);
    stringBuffer.append( method.getParamsForJavadoc() );
    stringBuffer.append(TEXT_27);
    stringBuffer.append( method.getReturnType() );
    stringBuffer.append(TEXT_28);
    stringBuffer.append( method.getName() );
    stringBuffer.append(TEXT_29);
    stringBuffer.append( method.getParamsForDeclaration() );
    stringBuffer.append(TEXT_30);
     
			String defaultReturnValue = method.getDefaultReturnValue();
			if (defaultReturnValue != null) { 

    stringBuffer.append(TEXT_31);
    stringBuffer.append( defaultReturnValue );
    stringBuffer.append(TEXT_32);
    
			} 

    stringBuffer.append(TEXT_33);
     
		}
	} 

     if (model.shouldGenInit()) { 
    stringBuffer.append(TEXT_34);
     } 
     if (model.shouldGenDestroy()) { 
    stringBuffer.append(TEXT_35);
     } 
     if (model.shouldGenGetPortletConfig()) { 
    stringBuffer.append(TEXT_36);
     } 
     if (model.shouldGenDoView()) { 
    stringBuffer.append(TEXT_37);
     } 
     if (model.shouldGenDoEdit()) { 
    stringBuffer.append(TEXT_38);
     } 
     if (model.shouldGenDoHelp()) { 
    stringBuffer.append(TEXT_39);
     } 
     if (model.shouldGenDoDispatch()) { 
    stringBuffer.append(TEXT_40);
     } 
     if (model.shouldGenRender()) { 
    stringBuffer.append(TEXT_41);
     } 
     if (model.shouldGenProcessAction()) { 
    stringBuffer.append(TEXT_42);
     } 
    stringBuffer.append(TEXT_43);
    stringBuffer.append(TEXT_44);
    return stringBuffer.toString();
  }
}
