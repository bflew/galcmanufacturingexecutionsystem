/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.service;

import java.util.List;

import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.domain_model.MasterData;

public interface AnimalService {

	/** load all animals that are currently on the market */
	List<Animal> getAllAnimals();

	/** 
	 * purchase the given animal, passing the passport number if 
	 * the value is above the threshold.  
	 * 
	 * @param animal the animal you want to buy
	 * @param passportNumber your passport number. if the price is above
	 * 		a threshold, then this is a mandatory field!
	 * @throws IdRequiredException if the passport number isnt supplied but is required.
	 * @throws AnimalDoesNotExistException if the animal was already sold.
	 */
	void purchaseAnimal(Animal animal, String passportNumber) throws IdRequiredException, AnimalDoesNotExistException;
	
	/** puts this animal on the market for anyone to purchase */
	void sellAnimal(Animal animal);
	
	/** called when a client starts up to get the master data */
	MasterData getMasterData();
}
