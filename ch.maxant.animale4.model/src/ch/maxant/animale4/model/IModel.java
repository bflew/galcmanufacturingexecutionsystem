/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.model;

import java.util.List;

import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.domain_model.MasterData;

/** 
 * this is a read only view of the model, as the view uses it.
 * views make changes to models via the controller, so that it can 
 * perform validation or server calls!
 */
public interface IModel {
	
	public static final String NAME = IModel.class.getName();

	/** event for when an animal is added to the model */
	String ADDED_ANIMALS = "added_animals";

	/** does the model need persisting? */
	boolean isDirty();

	/**
	 * @return a new unmodifiable list of animals
	 */
	List<Animal> getAnimals();

	/** returns the master data for this client */
	MasterData getMasterData();

}
