/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.model;

import java.beans.PropertyChangeListener;
import java.util.List;

import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.domain_model.MasterData;

/** 
 * this is the modifyable part of the model, which 
 * only the controller needs to know about.
 * the idea, is that the controller does all the 
 * stuff like validation before doing model updates.
 * the model is really simple and has simple setters
 * and getters.<br>
 * <br>
 * it is arguable whether this class belongs in the model bundle, or whether it belongs
 * in the core bundle.  it is the controller which requires it, so why couldn't the 
 * core bundle define this interface, and let OSGi DS ensure its available when the 
 * controller needs it?  The model bundle contains the model implementation, and both
 * its interfaces.  The service locator interface on the other hand is not in the 
 * bundle which provides the service locator implementation which is an OSGi DS.  
 * that is because the core bundle author is stating that they simply need the service
 * locator interface, and dont care who provides its implementation, so long as its 
 * available at runtime.
 */
public interface Model extends IModel {

	/** 
	 * the controller should add itself as a listener. anyone else interested in model 
	 * changes should ONLY use the EventBroker!
	 */
	void addPropertyChangeListener(PropertyChangeListener listener);
	
	/** 
	 * the controller may remove itself as a listener. anyone else interested in model 
	 * changes should ONLY use the EventBroker!
	 */
	void removePropertyChangeListener(PropertyChangeListener listener);
	
	/** the controller can call this to add an animal */
	void addAnimal(Animal animal);
	
	/** call this to set up the model, eg after loading the data from the server */
	void setAllAnimals(List<Animal> animals);

	/** called by the controller to set the master data upon login / startup */
	void setMasterData(MasterData masterData);

}
