/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.model.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.domain_model.MasterData;
import ch.maxant.animale4.model.Model;

/**
 * the purpose of this model is to handle eventing.
 * it contains the domain model, but itself is a client
 * only model.
 */
public class ModelImpl implements Model {

	private boolean dirty = false;
	
	private List<Animal> animals = new ArrayList<Animal>();
	
    /** Property support - stores and manages the actual properties. */
    protected transient PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	private MasterData masterData;

    /** {@inheritDoc} */
	public boolean isDirty() {
		return dirty;
	}

	/** any time the model would need persisting because of a change, call this method */
	private void setDirty() {
		this.dirty = true;
	}
	
	/** {@inheritDoc} */
	public void addPropertyChangeListener(PropertyChangeListener listener){
		pcs.addPropertyChangeListener(listener);
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(PropertyChangeListener listener){
		pcs.removePropertyChangeListener(listener);
	}

	/** {@inheritDoc} */
	public void addAnimal(Animal animal){
		setDirty();
		animals.add(animal);
		pcs.firePropertyChange(ADDED_ANIMALS, null, null);
	}
    
	/** {@inheritDoc} */
	public void setAllAnimals(List<Animal> animals){
		//not dirty, because this method is called when loading animals from the server
		this.animals = animals;
		pcs.firePropertyChange(ADDED_ANIMALS, null, null);
	}

	/** {@inheritDoc} */
	public List<Animal> getAnimals() {
		return Collections.unmodifiableList(animals);
	}

	/** {@inheritDoc} */
	@Override
	public MasterData getMasterData() {
		return masterData;
	}
	
	public void setMasterData(MasterData masterData) {
		this.masterData = masterData;
	}
	
}
