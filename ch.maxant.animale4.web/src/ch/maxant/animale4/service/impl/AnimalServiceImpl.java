/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.maxant.animale4.domain_model.Animal;
import ch.maxant.animale4.domain_model.Cat;
import ch.maxant.animale4.domain_model.Dog;
import ch.maxant.animale4.domain_model.MasterData;
import ch.maxant.animale4.service.AnimalDoesNotExistException;
import ch.maxant.animale4.service.AnimalService;
import ch.maxant.animale4.service.IdRequiredException;

public class AnimalServiceImpl implements AnimalService {

	/** the value, above which ID is required to make a purchase */
	private static final BigDecimal PRICE_THRESHOLD_WHERE_PURCHASES_REQUIRE_ID = new BigDecimal(
			"500.00");
	private ArrayList<Animal> animals;

	private static final Random RANDOM = new Random();

	public AnimalServiceImpl() {
		animals = new ArrayList<Animal>();

		@SuppressWarnings("unchecked")
		Class<? extends Animal>[] types = new Class[]{Dog.class, Cat.class};
		String[] names = new String[]{"Alfred", "Bella", "Charlie", "Dilbert", "Enricho", "Fred", "Gilbert", "Horus", "Ingrid", "Jackie"};
		for(int i = 0; i < names.length; i++){
			Class<? extends Animal> c = types[RANDOM.nextInt(types.length)];
			Animal a;
			try {
				a = c.newInstance();
				a.setName(names[i]);
				a.setAge(RANDOM.nextInt(7)+2);
				a.setPrice(new BigDecimal(RANDOM.nextInt(20000) + 40000).divide(new BigDecimal(100), BigDecimal.ROUND_HALF_DOWN));
				animals.add(a);
			} catch (Exception e) {
				//time to quit!
				throw new RuntimeException("failed to create an animal", e);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public List<Animal> getAllAnimals() {
		return animals;
	}
	

	/** {@inheritDoc} */
	@Override
	public MasterData getMasterData() {
		return new MasterData(PRICE_THRESHOLD_WHERE_PURCHASES_REQUIRE_ID);
	}

	/** {@inheritDoc} */
	@Override
	public void purchaseAnimal(Animal animal, String passportNumber) throws IdRequiredException, AnimalDoesNotExistException {
		
		//is it still available?
		if(animals.remove(animal)){
			if (animal.getPrice().doubleValue() > PRICE_THRESHOLD_WHERE_PURCHASES_REQUIRE_ID.doubleValue()) {
				if (passportNumber == null || passportNumber.trim().length() == 0) {

					//oh, not purchasing after all!
					animals.add(animal);
					
					System.err.println("\r\n\r\nPassport number is mandatory for purchases above "
							+ PRICE_THRESHOLD_WHERE_PURCHASES_REQUIRE_ID
							+ "!\r\n");
					
					throw new IdRequiredException(PRICE_THRESHOLD_WHERE_PURCHASES_REQUIRE_ID);
				}
			}

			System.out.println("\r\n\r\nYou now own a "
					+ animal.getClass().getName() + " called " + animal.getName()
					+ " - passportNumer was " + passportNumber + "\r\n");
		}else{
			throw new AnimalDoesNotExistException(animal);
		}

	}

	/** {@inheritDoc} */
	@Override
	public void sellAnimal(Animal a) {
		
		//create a copy, so that its ID is set correctly
		//if the client instantiated it, the ID may not be unique!
		Animal animal = null;
		try {
			animal = a.getClass().newInstance();
		} catch (InstantiationException e) {
			//should NEVER happen
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			//should NEVER happen
			throw new RuntimeException(e);
		}
		animal.setAge(a.getAge());
		animal.setName(a.getName());
		animal.setPrice(a.getPrice());

		animals.add(animal);
	}
}
