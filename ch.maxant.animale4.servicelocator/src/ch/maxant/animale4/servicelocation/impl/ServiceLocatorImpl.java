/*  
 * Copyright (c) 2011 Ant Kutschera
 * 
 * This file is part of Ant Kutschera's blog, 
 * http://blog.maxant.co.uk
 * 
 * This is free software: you can redistribute
 * it and/or modify it under the terms of the
 * Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the Lesser GNU General Public License for
 * more details. 
 * 
 * You should have received a copy of the
 * Lesser GNU General Public License along with this software.
 * If not, see http://www.gnu.org/licenses/.
 */
package ch.maxant.animale4.servicelocation.impl;

import ch.maxant.animale4.servicelocation.IServiceLocator;
import ch.maxant.comms.client.ClientConfiguration;
import ch.maxant.comms.client.ServiceFactory;
import ch.maxant.comms.common.SerializationType;

/**
 * this class implements the {@link IServiceLocator}.
 * this bundle declares this implementation so that its
 * loaded into the OSGi stack, so that the e4 EAP 
 * can inject it where required, in this case in the controller.
 * <br><br>
 * The controller doesnt care about the implementation, rather its the 
 * deployer who cares.  they could choose to stick the real deal in,
 * or some kind of stub for testing purposes.
 */
public class ServiceLocatorImpl implements IServiceLocator {

	private ServiceFactory factory;

	@Override
	public <T> T getService(Class<T> clazz) {
		ClientConfiguration config = new ClientConfiguration();
		config.setHost("localhost");
		config.setPort(8080);
		config.setPath("/ch.maxant.animale4.web/maxantCommsEndpoint");
		config.setSerializationType(SerializationType.JAVA_SERIALIZATION);
		return (T) factory.createServiceProxy(clazz, config);
	}
	
	public void bind(ServiceFactory factory){
		this.factory = factory;
	}
	
	public void unbind(ServiceFactory factory){
		if(this.factory == factory){
			this.factory = null;
		}
	}

}
